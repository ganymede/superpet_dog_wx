import { Pet } from "../model/pet.js"
//全局内存缓存对象
const CACHE = {
  accessToken: undefined,
  weRunLastTime: undefined,
  userInfo: undefined,
  isIndexFirstIn:true,
  userPetCount:0
}
module.exports = {
  CACHE: CACHE
}
